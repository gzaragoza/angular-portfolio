import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Projects } from './../interfaces/projects-response';

@Injectable({
  providedIn: 'root',
})
export class ProjectsService {
  constructor(private http: HttpClient) {}

  // set up that the response is a specific type withObservable<Type>
  getProjects(): Observable<Projects[]> {
    return this.http.get<Projects[]>('./../assets/json/projects.json');
  }
}
