export interface Projects {
  id: number;
  thumb: string[];
  img_home: string[];
  video_case: string;
  images: string[];
  project_type: string;
  technologies: string;
  project_name: string;
  role: string;
  client: string;
  agency: string;
  prices: string;
  description: string;
  problem: string;
  solution: string;
  links: Links[];
  bgcolor: string;
  font_color: string;
}

export interface Links {
  description: string;
  url: string;
}
