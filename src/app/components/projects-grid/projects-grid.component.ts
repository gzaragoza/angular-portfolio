import { Component, Input, OnInit } from '@angular/core';
import { Projects } from './../../interfaces/projects-response';
import { Router } from '@angular/router';
import { ProjectsService } from './../../services/projects.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-projects-grid',
  templateUrl: './projects-grid.component.html',
  styleUrls: ['./projects-grid.component.css'],
})
export class ProjectsGridComponent implements OnInit {
  @Input() projects: Projects[];

  constructor(private router: Router) {}

  onMovieClick(project: Projects) {
    this.router.navigate(['/project', project.id]);
  }

  ngOnInit(): void {
    // console.log('Data from ProjectsGridComponent', this.projects);
  }
}
