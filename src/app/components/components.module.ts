import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectsGridComponent } from './projects-grid/projects-grid.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { FooterComponent } from './footer/footer.component';
import { RouterModule } from '@angular/router';
import { ProjectsSlideShowComponent } from './projects-slide-show/projects-slide-show.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

@NgModule({
  declarations: [
    ProjectsGridComponent,
    NavBarComponent,
    FooterComponent,
    ProjectsSlideShowComponent,
  ],
  exports: [
    ProjectsGridComponent,
    NavBarComponent,
    FooterComponent,
    ProjectsSlideShowComponent,
  ],
  imports: [CommonModule, RouterModule, FontAwesomeModule],
})
export class ComponentsModule {}
