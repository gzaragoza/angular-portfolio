import { Component, Input, OnInit, AfterContentInit } from '@angular/core';
import { Projects } from './../../interfaces/projects-response';
import Swiper from 'swiper/bundle';
import 'swiper/swiper-bundle.css';

@Component({
  selector: 'app-projects-slide-show',
  templateUrl: './projects-slide-show.component.html',
  styleUrls: ['./projects-slide-show.component.css'],
})
export class ProjectsSlideShowComponent implements OnInit, AfterContentInit {
  @Input() projects: Projects[];

  constructor() {}

  ngAfterContentInit(): void {
    const swiper = new Swiper('.swiper-container', {
      // Optional parameters
      direction: 'horizontal',
      loop: true,
      observer: true,
      observeParents: true,

      // If we need pagination
      pagination: {
        el: '.swiper-pagination',
      },

      // Navigation arrows
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    });
  }
  ngOnInit(): void {}
}
