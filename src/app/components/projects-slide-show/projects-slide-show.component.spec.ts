import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectsSlideShowComponent } from './projects-slide-show.component';

describe('ProjectsSlideShowComponent', () => {
  let component: ProjectsSlideShowComponent;
  let fixture: ComponentFixture<ProjectsSlideShowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectsSlideShowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectsSlideShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
