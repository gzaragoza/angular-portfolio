import { Component, OnInit } from '@angular/core';
import { ProjectsGridComponent } from 'src/app/components/projects-grid/projects-grid.component';
import { ProjectsService } from './../../services/projects.service';
import { Projects } from './../../interfaces/projects-response';
@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css'],
})
export class NavBarComponent implements OnInit {
  public projects: Projects[] = [];
  constructor(private projectsService: ProjectsService) {
    this.projectsService.getProjects().subscribe((resp) => {
      this.projects = resp;
    });
  }
  navIsOpen = true;
  openClose = () => {
    if (this.navIsOpen) {
      document.getElementById('mySidenav').style.right = '0';
      document.getElementById('burgerIcon').style.display = 'none';
      document.getElementById('closeIcon').style.display = 'block';
    } else {
      document.getElementById('mySidenav').style.right = '-350px';
      document.getElementById('burgerIcon').style.display = 'block';
      document.getElementById('closeIcon').style.display = 'none';
    }

    return (this.navIsOpen = !this.navIsOpen);
  };

  ngOnInit(): void {}
}
