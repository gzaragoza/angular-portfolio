import { Component, OnInit } from '@angular/core';
import { ProjectsGridComponent } from 'src/app/components/projects-grid/projects-grid.component';
import { ProjectsService } from './../../services/projects.service';
import { Projects } from './../../interfaces/projects-response';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  public projects: Projects[] = [];

  constructor(private projectsService: ProjectsService) {
    this.projectsService.getProjects().subscribe((resp) => {
      this.projects = resp;
    });
  }

  ngOnInit(): void {}
}
