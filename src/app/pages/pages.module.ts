import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComponentsModule } from './../components/components.module';
import { HomeComponent } from './home/home.component';
import { ProjectComponent } from './project/project.component';
import { SafePipe } from './../pipes/safePipe';
@NgModule({
  declarations: [HomeComponent, ProjectComponent, SafePipe],
  imports: [CommonModule, ComponentsModule],
})
export class PagesModule {}
