import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProjectsService } from './../../services/projects.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Projects } from './../../interfaces/projects-response';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css'],
})
export class ProjectComponent implements OnInit {
  public projects: Projects[] = [];

  constructor(
    private activateRoute: ActivatedRoute,
    private projectService: ProjectsService,
    private location: Location,
    private projectsService: ProjectsService
  ) {}
  goBack() {
    this.location.back();
  }

  ngOnInit(): void {
    this.activateRoute.params.subscribe((routeParams) => {
      const { id } = this.activateRoute.snapshot.params;
      this.projectsService.getProjects().subscribe((resp) => {
        this.projects = resp.filter((project) => project.id === parseInt(id));
        console.log('this.projects', this.projects);
      });
    });
  }
}
